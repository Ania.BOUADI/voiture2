package garage;

import main.CompteCourant;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProprietaireTest {
    Voiture voiture = new Voiture(5, 200);
    Proprietaire p1= new Proprietaire("LE PROPRIETAIRE", voiture, 1000,2 );
    Voiture voiture2 = new Voiture(5, 200);


    @Test
    public void louerTest0() {
        assertEquals(voiture.estLouee(),false);
    }

    @Test
    public void louerTest() {
        Proprietaire p2 = new Proprietaire("Loueur",voiture2, 2000, 20);

        voiture2.setConducteur(p2);

        p2.setCompte(145);
        p2.getCompte().setSolde(100);

        boolean b =  p1.louer(voiture2, 120);
        assertEquals(voiture2.estLouee(),true);
    }


    @Test
    public void getCompteTest() {
        p1.setCompte(123184161);
        p1.getCompte().setSolde(10000);
        int solde_prop1= p1.getCompte().getSolde();
        assertEquals(solde_prop1,10000);
    }




}