package VOITURE2;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import static org.junit.Assert.*;
import garage.*;

public class Rendu {
    private Proprietaire proprietaire;
    private Voiture voitureLouee;
    private int numero;
    private boolean voitureLibre;

    @Given("Le propriétaire {string} a une voiture louée par {string}")
    public void le_proprietaire_a_une_voiture_louee(String nom, String nomLoueur){
        proprietaire = new Proprietaire( nom, new Voiture(0, 3), 1000, 10);
        voitureLouee = new Voiture(4, 200);
        voitureLouee.setConducteur(new Proprietaire(nomLoueur, voitureLouee, 1000, 10));
        proprietaire.louer(voitureLouee, 100);
    }

    @When("il veut la rendre")
    public void il_veut_la_rendre(){
        voitureLibre = proprietaire.rendreLocation();
    }

    @Then("la voiture redevient non-louée et il ne la loue plus")
    public void la_voiture_redevient_non_louee_et_il_ne_la_loue_plus() {
        assert(voitureLibre == false);
    }

}
