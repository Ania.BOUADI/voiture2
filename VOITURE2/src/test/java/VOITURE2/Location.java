package VOITURE2;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import static org.junit.Assert.*;
import garage.*;

public class Location {
    private Proprietaire proprietaire;
    private Voiture voitureLouee;
    private int numero;

    @Given("Le propriétaire {string} existe et a une voiture avec {int} et {int} km, avec un prix de {int} et une duree {int}")
    public void le_proprietaire_a_besoin_d_une_voiture(String nom, int nbportesDeSaVoiture, int kmDeSaVoiture, int prixAchat, int duree){
        proprietaire = new Proprietaire(nom, new Voiture(nbportesDeSaVoiture, kmDeSaVoiture), prixAchat, duree);
    }

    @When("une voiture non-louée lui est proposée avec {int} portes et {int} km")
    public void une_voiture_non_louee_lui_est_proposee(int nbportesLouee, int kmLouee){
        voitureLouee = new Voiture(nbportesLouee, kmLouee);
    }

    @Then("la voiture lui est assignée de {string} avec un prix {int}, une duree {int}, numero de compte {int} en location à un prix de {int} euros.")
    public void la_voiture_lui_est_assignee_en_location(String nomLoueur, int prix, int dureeLoueur, int numeroCompte, int prixLoc) {
        voitureLouee.setConducteur(new Proprietaire(nomLoueur, voitureLouee, prix, dureeLoueur));
        voitureLouee.getConducteur().setCompte(numeroCompte);
        proprietaire.louer(voitureLouee, prixLoc);
    }

}
