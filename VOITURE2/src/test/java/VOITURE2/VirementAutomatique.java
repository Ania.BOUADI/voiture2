package VOITURE2;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import static org.junit.Assert.*;
import garage.*;

public class VirementAutomatique {
    private Proprietaire proprietaire;
    private Voiture voitureLouee;
    private int numero;
    private int argentLoueur;

    @Given("Le proprio {string} a besoin d'une voiture")
    public void le_proprio_a_besoin_d_une_voiture(String nom){
        proprietaire = new Proprietaire(nom, new Voiture(0, 3), 1000, 10);
    }

    @When("une voitur non-louée lui est proposée")
    public void une_voiture_lui_est_proposee(){
        voitureLouee = new Voiture(4, 200);
    }

    @Then("il paie la voiture de {string} qui a une solde de {int} et la voiture lui est assignée pour un prix de {int}")
    public void il_paie_la_voiture_et_la_voiture_lui_est_assignee(String nomLoueur, int soldeLoueur, int prixLocation) {
        if(proprietaire == null){
            proprietaire = new Proprietaire("jean", new Voiture(0, 3), 1000, 10);
        }
        voitureLouee.setConducteur(new Proprietaire(nomLoueur, voitureLouee, 1000, 10));
        voitureLouee.getConducteur().setCompte(145);
        voitureLouee.getConducteur().getCompte().setSolde(soldeLoueur);
        proprietaire.louer(voitureLouee, prixLocation);
        int argentLoueurApres = voitureLouee.getConducteur().getCompte().getSolde();
        assert(argentLoueurApres == (soldeLoueur+prixLocation));
    }


}

