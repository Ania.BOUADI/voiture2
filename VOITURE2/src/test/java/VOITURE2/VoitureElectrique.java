package VOITURE2;

import garage.Proprietaire;
import garage.Voiture;
import garage.VoitureDecoree;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class VoitureElectrique {
    private Voiture voiture;
    private VoitureDecoree voitureElectrique;

    @Given("Le propriétaire a une voiture électrique avec {int} portes et {int} km")
    public void a(int portes, int km){
        voiture = new Voiture(4, 10000);
    }

    @When("il crée sa voiture électrique avec une autonomie de {int} et roule {int}")
    public void creer_voiture_electrique(int autonomie, int rouler){
        voitureElectrique = new VoitureDecoree(voiture,345);
        voitureElectrique.rouler(rouler);
    }

    @Then("la voiture est crée avec sa méthode adaptée et autonomie restante {int}")
    public void test_voiture_electrique(int reste) {
        assert (voitureElectrique.autonomieRestante() == reste);
    }
}
