package VOITURE2;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import static org.junit.Assert.*;
import garage.Voiture;

class TestKm {
    static int get_km(Voiture voiture){
        return voiture.getKm();
    }
}

public class Stepsdefs {
    private Voiture voiture0;
    private int  actualAnswer;

    @Given("la voiture a roulé {int} et contient déja un compteur de kilométrage depuis sa fabrication {int}")
    public void set_voiture_km(int km,int nb_km) {
        voiture0 = new Voiture(5,km);
        voiture0.rouler(nb_km);
    }

    @When("le conducteur demande quel est le nombre total de kilometre")
    public void get_nombre_km() {
        actualAnswer = TestKm.get_km(this.voiture0);
    }

    @Then("le nombre de kilomètre total parcouru par la voiture est calculé {int}")
    public void retourner_nbkm(int expectedAnswer) {
        assertEquals(expectedAnswer, actualAnswer);
    }

}
