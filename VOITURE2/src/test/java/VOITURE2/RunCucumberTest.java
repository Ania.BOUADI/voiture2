package VOITURE2;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"},strict = true,features="src/test/resources/VOITURE2/")
public class RunCucumberTest {
}
