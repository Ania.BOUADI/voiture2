Feature: US 01 Calcul kilométrage
  En tant que conducteur
  Je veux savoir après avoir roulé avec la voiture, le nombre de kilomètre parcouru par la voiture
  Dans le but d'estimer sa valeur sur le marché

  Scenario Outline: le calcul du nombre de kilomètre parcouru par la voiture
    Given la voiture a roulé <nb_km> et contient déja un compteur de kilométrage depuis sa fabrication <km>
    When le conducteur demande quel est le nombre total de kilometre
    Then le nombre de kilomètre total parcouru par la voiture est calculé <nb_total>
    Examples:
      | nb_km | km | nb_total
      | 2     | 45 | 47
      | 500   | 48 | 548




