Feature: US 07 Virement automatique
  En tant que Propriétaire
  Je veux pouvoir payer automatique ma location
  Dans le but de me déplacer avec, lorsque je suis en vacances.

  Scenario Outline: Le proprio a une voiture de location payée
    Given Le proprio <nom> a besoin d'une voiture
    When une voitur non-louée lui est proposée
    Then il paie la voiture de <nomLoueur> qui a une solde de <soldeLoueur> et la voiture lui est assignée pour un prix de <prixLocation>
    Examples:
      | nom | nomLoueur | soldeLoueur | prixLocation |
      | "RE"| "YUYU"    | 145         | 50           |
      | "YU"| "RE"      | 45555       | 800          |
