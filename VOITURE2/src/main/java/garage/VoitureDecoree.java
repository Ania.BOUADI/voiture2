package garage;
// Voiture enrichie, voiture électrique

public class VoitureDecoree implements VoitureInterface{
	private Voiture voituredecoree;
	private int autonomie; // autonomie d'une voiture éléctrique
	
	public VoitureDecoree() {
		voituredecoree=null;
		autonomie=0;
	}
	public VoitureDecoree(Voiture voituredecoree, int autonomie) {
		super();
		this.voituredecoree = voituredecoree;
		this.autonomie = autonomie;
	}
	@Override
	public boolean estLouee() {
		return voituredecoree.estLouee();
	}
	
	@Override
	public int rouler(int km) {
		autonomie = (int) (autonomie - km / 5);
		return voituredecoree.rouler(km);
	}
	public int autonomieRestante(){
		return autonomie;
	}


}
