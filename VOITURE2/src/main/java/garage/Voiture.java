package garage;

public class Voiture implements VoitureInterface
{
    // variables d'instance
    private int nbportes;
    private int km;
    private boolean louee;
    /* Deuxieme conducteur */
    private Proprietaire conducteur;
    private int prixLocation;

    /**
     * Constructeur d'objets de classe Voiture
     */
    public Voiture(int nbportes, int km)
    {
        // initialisation des variables d'instance
        this.nbportes = nbportes;
        this.km = km;
        this.conducteur = null;
        this.louee = false;
    }

    //Retourner le nombre de portes de la voiture
    public int getNbportes(){
        return this.nbportes;
    }

    public boolean estLouee() {
        return louee;
    }

    public int getPrixLocation() {
        return this.prixLocation;
    }

    public int setPrixLocation(int prixLocation) {
        this.prixLocation = prixLocation;
        return this.prixLocation;
    }

    //Retourner le conducteur de la voiture
    public Proprietaire getConducteur() {
        return conducteur;
    }

    //Modifier le conducteur de la voiture
    public void setConducteur(Proprietaire conducteur) {
        this.conducteur = conducteur;
    }

    //Modifier le nombre de portes de la voiture
    private void setNbportes(int nbportes){
        this.nbportes = nbportes;
    }

    //Retourner le kilomètrage de la voiture
    public int getKm(){
        return this.km;
    }

    //Modifier le kilomètrage de la voiture
    public void setKm(int km){
        this.km = km;
    }

    public void setLouee(boolean l) {
        this.louee = l;
    }

    //Changer le kilomètrage de la voiture après avoir rouler
    public int rouler(int km){
        this.km += km;
        return km;
    }
    
    
    // Voir si la voiture a rouler
}