package garage;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VoitureTest {
    private Voiture Clio;
    private Proprietaire Evine; // Propriétaire de la voiture Clio
    private Proprietaire Ania; // Deuxieme conducteur de la voiture Clio

    public VoitureTest()
    {
    }
    /**
     * Met en place les engagements.
     *
     * Méthode appelée avant chaque appel de méthode de test.
     */
    @Before
    public void setUp() // throws java.lang.Exception
    {	 // null car ne possède pas de voiture
        Ania = new Proprietaire("Ania", null, 0,0 );
        Clio = new Voiture(4,10);
        Evine = new Proprietaire("Evine",Clio, 10000, 2);
        Clio.setConducteur(Ania);
    }

    /**
     *
     * Méthode appelée après chaque appel de méthode de test.
     */
    @After
    public void tearDown() // throws java.lang.Exception
    {
        //Libérez ici les ressources engagées par setUp()
    }

    @Test
    public void TestDeuxiemeConducteur(){
        assertEquals( Ania, Clio.getConducteur());

    }

    @Test
    public void TestTroisiemeConducteur(){
        Proprietaire Evnia = new Proprietaire("Evnia", null, 0,0 );
        Clio.setConducteur(Evnia);
        assertEquals( Evnia, Clio.getConducteur());

    }
    // Ania devient proprietaire de la Clio
    @Test
    public void TestChangementProprietaire(){
        Ania.setVoiture(Clio);
        Evine.setVoiture(null);
        assertEquals(Clio, Ania.getVoiture() );
        assertEquals(null, Evine.getVoiture());
    }

    @Test
    public void TestRouler()
    {
        Voiture Clio = new Voiture(4, 30);
        assertEquals(50, Clio.rouler(20));
    }



    @Test
    public void TestCalculPrixDeVente()
    {
        assertEquals(4545, Evine.prixVenteVoiture(20));
    }

}