Feature: US 06 Rendu de location
  En tant que Propriétaire
  Je veux rendre une voiture louée par mes soins
  Dans le but de ne plus l'avoir

  Scenario Outline: Le propriétaire a une voiture de location
    Given Le propriétaire <nom> a une voiture louée par <nomLoueur>
    When il veut la rendre
    Then la voiture redevient non-louée et il ne la loue plus
    Examples:
      | nom | nomLoueur |
      | "RT"| "TYYYY"   |


