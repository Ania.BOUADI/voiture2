Feature: US 05 Location
  En tant que Propriétaire
  Je veux pouvoir louer une voiture
  Dans le but de me déplacer avec, lorsque je suis en vacances.

  Scenario Outline: Le propriétaire a une voiture de location
    Given Le propriétaire <nom> existe et a une voiture avec <nbportesDeSaVoiture> et <kmDeSaVoiture> km, avec un prix de <prixAchat> et une duree <duree>
    When une voiture non-louée lui est proposée avec <nbportesLouee> portes et <kmLouee> km
    Then la voiture lui est assignée de <nomLoueur> avec un prix <prix>, une duree <dureeLoueur>, numero de compte <numeroCompte> en location à un prix de <prixLoc> euros.
    Examples:
      | nom | nbportesDeSaVoiture | kmDeSaVoiture | prixAchat | duree | nbportesLouee | kmLouee | nomLoueur | prix | dureeLoueur | numeroCompte | prixLoc|
      | "Jean" | 3                | 500           | 10000     | 4     |4              |145      | "JeanValJean"| 10| 123         |56            | 120    |
      | "René" | 2                | 40            | 1345      | 45    | 3             | 56      | "Coty"       | 45| 456         |32            | 60     |

