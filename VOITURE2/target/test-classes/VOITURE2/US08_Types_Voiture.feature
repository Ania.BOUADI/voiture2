Feature: US 08 Mon Type de voiture
  En tant que Propriétaire
  Je veux pouvoir avoir une voiture spéciale
  Dans le but d'utiliser le système

  Scenario Outline: Le propriétaire a une voiture
    Given Le propriétaire a une voiture électrique avec <portes> portes et <km> km
    When il crée sa voiture électrique avec une autonomie de <autonomie> et roule <rouler>
    Then la voiture est crée avec sa méthode adaptée et autonomie restante <reste>
    Examples:
      | portes | km | autonomie | rouler | reste |
      | 3      | 34 | 345       | 45     | 300   |
      | 7      | 566| 679       | 200    | 479   |
